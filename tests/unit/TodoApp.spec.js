import { mount } from "@vue/test-utils";
import TodoApp from "@/components/example/TodoApp.vue";

describe("TodoApp.vue", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(TodoApp);
  });

  it("Should render todo text", () => {
    expect(wrapper.get('[data-test="todo"]').text()).toBe("Learn Vue Testing");
  });

  it("Should add new todo", async () => {
    // expect default length = 1
    expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1);

    // set input value
    await wrapper.get('[data-test="new-todo"]').setValue("New Todo");

    // trigger form submit
    await wrapper.get('[data-test="form-todo"]').trigger("submit");

    // expect todo have length 2
    expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(2);

    // clear input after submit
    await wrapper.get('[data-test="new-todo"]').setValue("");

    expect(wrapper.get('[data-test="new-todo"]').text()).toBe("");
  });

  it("should be able to completed todo", async () => {
    await wrapper.get('[data-test="todo-checkbox"]').setChecked(true);

    expect(wrapper.get('[data-test="todo"]').classes()).toContain("completed");
  });
});
