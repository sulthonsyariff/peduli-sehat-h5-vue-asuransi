import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
// language
import i18n from "./plugins/vue-i18n";
// social media sharing
import VueSocialSharing from "vue-social-sharing";
// more plugins
import "./plugins";
// import mixins loading for global variable
import { showLoading, hideLoading } from "./mixins/loading";
// import mixins notification or alert for global variable
import { showNotification } from "./mixins/utils";
// google analytics
import VueGtag from "vue-gtag";

// global component
import BaseNotification from "@/components/Base/Notification";
import BaseLoading from "@/components/Base/Loading";

// otp input
import OtpInput from "@bachdgvn/vue-otp-input";

import "@/assets/css/tailwind.css"; // tailwind styles
import "@/assets/css/custom.scss"; // custom styles

Vue.config.productionTip = false;

let idGtag =
  process.env.VUE_APP_BASE_DOMAIN === "pre" ? "G-V1ZPJJ72LK" : "UA-127450137-1";

Vue.use(
  VueGtag,
  {
    config: { id: idGtag }
  },
  router
);

Vue.use(VueSocialSharing);

Vue.component("base-notification", BaseNotification);
Vue.component("base-loading", BaseLoading);
Vue.component("v-otp-input", OtpInput);

// loading
Vue.prototype.$showLoading = showLoading;
Vue.prototype.$hideLoading = hideLoading;

// notification
Vue.prototype.$showNotification = showNotification;

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
