// vue-cookies
import "../plugins/vue-cookies";

export function getBrowserDevice() {
  let result;
  if (
    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )
  ) {
    result = "Android-H5";
  } else {
    result = "H5";
  }
  return result;
}

export function browserVersion() {
  let u = navigator.userAgent;
  return {
    trident: u.indexOf("Trident") > -1, //IE内核
    presto: u.indexOf("Presto") > -1, //opera内核
    webKit: u.indexOf("AppleWebKit") > -1, //苹果、谷歌内核
    gecko: u.indexOf("Gecko") > -1 && u.indexOf("KHTML") == -1, //火狐内核
    mobile: !!u.match(/AppleWebKit.*Mobile.*/) || u.indexOf("moslem") > -1, //是否为移动终端 (Moslem APP因为要解决app内Google登录的原因，除去了AppleWebKit.*Mobile这些标记)
    ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
    androidEnd: u.indexOf("Android") > -1 || u.indexOf("Adr") > -1, //android终端

    android: u.indexOf("language") > -1, //peduli APP应用
    moslemApp: u.indexOf("moslem") > -1, //Moslem APP应用
    androids: u.indexOf("language") > -1 || u.indexOf("moslem") > -1, // peduli APP应用 + Moslem APP应用

    iPhone: u.indexOf("iPhone") > -1, //是否为iPhone或者QQHD浏览器
    iPad: u.indexOf("iPad") > -1, //是否iPad
    webApp: u.indexOf("Safari") == -1, //是否web应该程序，没有头部与底部
    weixin: u.indexOf("MicroMessenger") > -1, //是否微信 （2015-01-22新增）
    qq: u.match(/\sQQ/i) == " qq", //是否QQ
    UC: u.indexOf("UCBrowser") > -1, //UC浏览器
    isLine: u.indexOf("Line") > -1 //Line中
  };
}

export function amountFormat(number) {
  return Number(number).toLocaleString(["ban", "id"]);
}

export function goToLogin() {
  // eslint-disable-next-line no-undef
  $cookies.remove("passport");

  setTimeout(() => {
    // let appLink = "qsc://app.pedulisehat/go/login?req_code=600";

    // if (getBrowserDevice() === "H5") {
    location.href =
      "https://" +
      process.env.VUE_APP_BASE_DOMAIN +
      ".pedulisehat.id/login.html?qf_redirect=" +
      encodeURIComponent(location.href);
    // } else {
    //   location.href = appLink;
    // }
  }, 30);
}

export function showNotification(message) {
  this.$store.dispatch("general/showNotification", {
    message: message
  });
}

export function getAge(birthDateString) {
  let today = new Date();
  let birthDate = new Date(birthDateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }

  return age;
}

export function monthDiff(d1, d2) {
  let months;
  d1 = new Date(d1);
  d2 = new Date(d2);
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= d1.getMonth();
  months += d2.getMonth();

  return months <= 0 ? 0 : months;
}

// 00 = january
// ...
// 11 = december
export function isValidDate(year, month, day) {
  var d = new Date(year, month, day);
  if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
    return true;
  }
  return false;
}

export function reverseDate(date) {
  let result = date
    .split("-")
    .reverse()
    .join("-");

  return result;
}
