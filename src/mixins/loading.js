export function showLoading() {
  this.$store.dispatch("general/showLoading");
}

export function hideLoading() {
  this.$store.dispatch("general/hideLoading");
}
