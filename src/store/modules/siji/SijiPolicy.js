const namespaced = true;

const state = {
  commodityItem: null,
  policyId: null,
  pInsId: null,
  form: {
    policyHolder: {
      full_name: "",
      id_card: null,
      email: "",
      mobile_number: "",
      gender: null,
      province_name: "",
      city_name: "",
      postal_code: "",
      address: "",
      commodity_item: null,
      tax_number: ""
    },
    insuredPerson: {
      policy_id: null,
      full_name: "",
      id_card: null,
      email: "",
      mobile_number: "",
      gender: null,
      province_name: "",
      city_name: "",
      postal_code: "",
      address: "",
      relationship: null,
      locale: "id"
    },
    policyBeneficiary: [
      {
        beneficiaries: [
          {
            full_name: "",
            gender: null,
            relationship: null,
            hari: "",
            bulan: "",
            tahun: "",
            birth_date: ""
          }
        ]
      }
    ]
  }
};

const getters = {
  commodityItem: state => state.commodityItem,
  policyId: state => state.policyId,
  pInsId: state => state.pInsId,
  policyHolder: state => state.form.policyHolder,
  insuredPerson: state => state.form.insuredPerson,
  policyBeneficiary: state => state.form.policyBeneficiary,
  insured: state => state.form.insured,
  daftarAlamat: state => state.form.daftarAlamat,
  familyCard: state => state.form.familyCard,
  payment: state => state.form.payment,
  form: state => state.form,
  orderId: state => state.orderId,
  summaryFinal: state => state.summaryFinal
};

const mutations = {
  SET_COMMODITY_ITEM: (state, payload) => {
    state.commodityItem = payload;
  },
  SET_POLICY_ID: (state, payload) => {
    state.policyId = payload;
  },
  SET_P_INS_ID: (state, payload) => {
    state.pInsId = payload;
  },
  SET_POLICY_HOLDER: (state, payload) => {
    state.form.policyHolder = payload;
  },
  SET_INSURED_PERSON: (state, payload) => {
    state.form.insuredPerson = payload;
  },
  SET_POLICY_BENEFICIARY: (state, payload) => {
    state.form.policyBeneficiary = payload;
  },
  SET_ORDER_ID: (state, payload) => {
    state.orderId = payload;
  },
  SET_DAFTAR_ALAMAT: (state, payload) => {
    state.form.daftarAlamat = payload;
  },
  SET_FAMILY_CARD: (state, payload) => {
    state.form.familyCard = payload;
  },
  SET_INSURED: (state, payload) => {
    state.form.insured = payload;
  },
  SET_PAYMENT: (state, payload) => {
    state.form.payment = payload;
  },
  SET_SUMMARY_FINAL: (state, payload) => {
    state.summaryFinal = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchStatementHealth: ({ commit }) => {
    const url = `${process.env.VUE_APP_BASE_FRESHDESK}/api/v2/solutions/articles/43000582431`;
    return window
      .axios({
        method: "get",
        auth: {
          username: "cjeane@pedulisehat.id",
          password: "januari20"
        },
        url
      })
      .then(response => {
        return response.data.description;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchTermsConditions: ({ commit }) => {
    const url = `${process.env.VUE_APP_BASE_FRESHDESK}/api/v2/solutions/articles/43000582425`;
    return window
      .axios({
        method: "get",
        auth: {
          username: "cjeane@pedulisehat.id",
          password: "januari20"
        },
        url
      })
      .then(response => {
        return response.data.description;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchRiplayGeneral: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/siji/riplay/umum?sku_key=${data.sku_key}`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchRiplayPersonal: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/siji/riplay/personal`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchListProvince: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_BASE}/v1/regions?pid=0`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchListCity: ({ commit }, payload) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_BASE}/v1/regions?pid=${payload.cid}`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getPolicy: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/holder?policy_id=${data.policyId}`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  policyRegistration: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/holder`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getInsured: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/ins_person?policy_id=${data.policyId}`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  insuredRegistration: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/ins_person`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  getBeneficiary: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/beneficiary?policy_id=${data.policyId}`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  beneficiaryRegistration: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/beneficiary`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  beneficiarySubmitData: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy/submit_data_siji`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  makeOrder: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_order`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  makePayment: ({ commit }, payload) => {
    let data = payload;
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/ins_pay`,
        data: data
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
