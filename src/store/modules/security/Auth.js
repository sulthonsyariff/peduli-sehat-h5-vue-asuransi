const namespaced = true;

const state = {};

const getters = {};

const mutations = {};

const actions = {
  // eslint-disable-next-line no-unused-vars
  refreshToken: ({ commit }, payload) => {
    return window.axios
      .get(`${process.env.VUE_APP_BASE_PASSPORT}/v1/oauth2/token`, {
        params: payload
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
