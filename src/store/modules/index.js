import camelCase from "lodash/camelCase";

const modules = {};

// security
const requireSecurity = require.context("./security", false, /\.js$/);
requireSecurity.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireSecurity(fileName)
  };
});

// pedulicare
const requirePedulicare = require.context("./pedulicare", false, /\.js$/);
requirePedulicare.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requirePedulicare(fileName)
  };
});

// others
const requireOthers = require.context("./others", false, /\.js$/);
requireOthers.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireOthers(fileName)
  };
});

// siji
const requireSiji = require.context("./siji", false, /\.js$/);
requireSiji.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireSiji(fileName)
  };
});

// siji
const requireMyInsurance = require.context("./myInsurance", false, /\.js$/);
requireMyInsurance.keys().forEach(fileName => {
  if (fileName === "./index.js") return;
  const moduleName = camelCase(fileName.replace(/(\.\/|\.js)/g, ""));
  modules[moduleName] = {
    namespaced: true,
    ...requireMyInsurance(fileName)
  };
});

export default modules;
