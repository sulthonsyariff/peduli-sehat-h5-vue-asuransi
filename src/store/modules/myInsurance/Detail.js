const namespaced = true;

const state = {};

const getters = {
  policyHolder: state => state.form.policyHolder
};

const mutations = {
  SET_ORDER_ID: (state, payload) => {
    state.policyHolder = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  fetchDetail: ({ commit }, payload) => {
    return window.axios
      .get(`${process.env.VUE_APP_BASE_PSUIC}/v1/ins_policy`, {
        params: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
