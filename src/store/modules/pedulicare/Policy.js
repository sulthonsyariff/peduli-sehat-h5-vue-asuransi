const namespaced = true;

const state = {
  summaryFinal: {
    noPolis: "",
    urlIseller: ""
  },
  form: {
    policy_period: "",
    policyHolder: {
      firstName: "",
      lastName: "",
      hari: "",
      bulan: "",
      tahun: "",
      dateOfBirth: "",
      maritalStatus: null,
      placeOfBirth: "",
      identityNumber: "",
      gender: null,
      title: "",
      phoneNumber: "",
      email: ""
    },
    daftarAlamat: [
      {
        address: "",
        type: 1,
        postalCode: "",
        kecamatan: "",
        kelurahan: "",
        kota: "",
        province: "",
        country: "",
        telephones: [
          { type: 1, number: "" },
          { type: 6, number: "" }
        ]
      }
    ],
    payment: {
      period: "A",
      policy_period: "",
      premiTotal: "",
      paymentTotal: ""
    },
    familyCard: {
      file: "",
      ext: "",
      base64: ""
    },
    insured: [
      {
        documents: [
          {
            category: "KTP",
            memo: "",
            file: "",
            ext: "",
            base64: ""
          }
        ],
        fullName: "",
        phoneNumber: "",
        identityNumber: "",
        hari: "",
        bulan: "",
        tahun: "",
        dateOfBirth: "",
        gender: null,
        maritalStatus: null,
        relation: 1,
        email: "",
        heirs: [
          {
            fullName: "",
            hari: "",
            bulan: "",
            tahun: "",
            dateOfBirth: "",
            relation: null,
            gender: null,
            percentage: null,
            phoneNumber: ""
          }
        ]
      }
    ]
  }
};

const getters = {
  policyHolder: state => state.form.policyHolder,
  insured: state => state.form.insured,
  daftarAlamat: state => state.form.daftarAlamat,
  familyCard: state => state.form.familyCard,
  payment: state => state.form.payment,
  form: state => state.form,
  orderId: state => state.orderId,
  summaryFinal: state => state.summaryFinal
};

const mutations = {
  SET_ORDER_ID: (state, payload) => {
    state.orderId = payload;
  },
  SET_POLICY_HOLDER: (state, payload) => {
    state.form.policyHolder = payload;
  },
  SET_DAFTAR_ALAMAT: (state, payload) => {
    state.form.daftarAlamat = payload;
  },
  SET_FAMILY_CARD: (state, payload) => {
    state.form.familyCard = payload;
  },
  SET_INSURED: (state, payload) => {
    state.form.insured = payload;
  },
  SET_PAYMENT: (state, payload) => {
    state.form.payment = payload;
  },
  SET_SUMMARY_FINAL: (state, payload) => {
    state.summaryFinal = payload;
  }
};

const actions = {
  // eslint-disable-next-line no-unused-vars
  policyRegistration: ({ commit }, payload) => {
    let data = payload;

    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/policy_registration`,
        data: data
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchCalculate: ({ commit }, payload) => {
    let data = payload;

    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/policy_calculate`,
        data: data
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchListPaymentMethod: ({ commit }) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_TRADE}/v1/pay_channel/pt`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  ktpParsing: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/ktp`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  uploadFile: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/upload`,
        data: payload
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  fetchHomePage: ({ commit }, payload) => {
    return window
      .axios({
        method: "get",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/insurance-home`
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  checkKtpEligible: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/check-ktp-eligible`,
        data: payload
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  },

  // eslint-disable-next-line no-unused-vars
  checkPaymentStatus: ({ commit }, payload) => {
    return window
      .axios({
        method: "post",
        url: `${process.env.VUE_APP_BASE_PSUIC}/v1/psac/payment-status`,
        data: payload
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        throw error.response.data;
      });
  }
};

export { namespaced, state, getters, mutations, actions };
