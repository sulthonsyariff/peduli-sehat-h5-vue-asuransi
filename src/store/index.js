import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import modules from "./modules";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  key: "pedulisehat-asuransi",
  storage: window.localStorage,
  modules: ["policy", "sijiPolicy"]
});

export default new Vuex.Store({
  modules,
  plugins: [vuexLocal.plugin]
});
