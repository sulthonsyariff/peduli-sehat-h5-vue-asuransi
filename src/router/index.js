import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
let psacTitle = "Pedulicare Anti Corona";
let sijiTitle = "SiJi Corona Protection";

const routes = [
  // home page
  {
    path: "/",
    component: () => import("../views/Wrapper.vue"),
    children: [
      {
        path: "",
        name: "HomePage",
        component: () => import("../views/Asuransi/Index.vue")
      }
    ]
  },

  // myInsurance
  {
    path: "/my-insurance",
    component: () => import("../views/Wrapper.vue"),
    children: [
      {
        path: "policy-data/:policyId",
        name: "PolicyData",
        component: () => import("../views/Asuransi/MyInsurance/PolicyData.vue")
      },
      {
        path: "detail/:policyId",
        name: "PolicyDetail",
        component: () =>
          import("../views/Asuransi/MyInsurance/PolicyDetail.vue")
      }
    ]
  },
  // pedulicare anti corona
  {
    path: "/pedulicare",
    component: () => import("../views/Wrapper.vue"),
    children: [
      {
        path: "",
        name: "PedulicareHome",
        component: () => import("../views/Asuransi/Pedulicare/Index.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "benefits",
        name: "PedulicareBenefits",
        component: () => import("../views/Asuransi/Pedulicare/Benefits.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "policy-exclusions",
        name: "PedulicarePolicyExclusions",
        component: () =>
          import("../views/Asuransi/Pedulicare/PolicyExclusions.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "claim-documents",
        name: "PedulicareClaimDocuments",
        component: () =>
          import("../views/Asuransi/Pedulicare/ClaimDocuments.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "policy-holder",
        name: "PedulicarePolicyHolderForm",
        component: () =>
          import("../views/Asuransi/Pedulicare/PolicyHolderForm.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "insured-data",
        name: "PedulicareInsuredDataForm",
        component: () =>
          import("../views/Asuransi/Pedulicare/InsuredDataForm.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "beneficiary-data",
        name: "PedulicareBeneficiaryDataForm",
        component: () =>
          import("../views/Asuransi/Pedulicare/BeneficiaryDataForm.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "summary",
        name: "PedulicareSummary",
        component: () => import("../views/Asuransi/Pedulicare/Summary.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "completed-payment/:status",
        name: "PedulicareCompletedPayment",
        component: () =>
          import("../views/Asuransi/Pedulicare/CompletedPayment.vue"),
        meta: {
          title: psacTitle
        }
      },
      {
        path: "summary-final",
        name: "PedulicareSummaryFinal",
        component: () =>
          import("../views/Asuransi/Pedulicare/SummaryFinal.vue"),
        meta: {
          title: psacTitle
        }
      }
    ]
  },
  // end pedulicare anti corona

  // siji corona
  {
    path: "/sijicorona",
    component: () => import("../views/Wrapper.vue"),
    children: [
      {
        path: "",
        name: "SijiCoronaHome",
        component: () => import("../views/Asuransi/Siji/Index.vue"),
        meta: {
          title: sijiTitle
        }
      },
      {
        path: "policy-holder/:policyId",
        name: "SijiPolicyHolderForm",
        component: () => import("../views/Asuransi/Siji/PolicyHolderForm.vue"),
        meta: {
          title: sijiTitle
        }
      },
      {
        path: "insured-data/:policyId",
        name: "SijiInsuredDataForm",
        component: () => import("../views/Asuransi/Siji/InsuredDataForm.vue"),
        meta: {
          title: sijiTitle
        }
      },
      {
        path: "beneficiary-data/:policyId/:popupPayment?",
        name: "SijiBeneficiaryDataForm",
        component: () =>
          import("../views/Asuransi/Siji/BeneficiaryDataForm.vue"),
        meta: {
          title: sijiTitle
        }
      },
      {
        path: "underwritingfailed",
        name: "UnderWritingFailed",
        component: () => import("../views/Asuransi/Siji/UnderWritingFailed.vue")
      }
    ]
  },
  // end siji corona

  // example unit test
  {
    path: "/todo-app",
    component: () => import("../views/Wrapper.vue"),
    children: [
      {
        path: "",
        name: "TodoApp",
        component: () => import("../components/example/TodoApp.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
